class Food{
  
  int x = 0;
  int y = 0;
  int w = 20;
  int h = 20;
  float onOrOff = random(1);
  boolean active;
  int green = 255;
  
  void show(){
    
    fill(0, green, 0); //green
    
    //there is a 20% chance that the food object will be green (edible)
    if(onOrOff > 0.8){
      this.active = true;
    } else {
      fill(0); //black
      this.active = false;
    }
    
    rect(x, y, w, h);
    
  }
  
  void setPosition(int x, int y){
    this.x = x;
    this.y = y;
  }
  
  int getX(){
    return this.x;
  }
  
  int getY(){
    return this.y;
  }
  
  void printPosition(){
    println("X:" + this.x + " Y:" + this.y);
  }
  
  void eaten(){
    this.green = 0;
  }
  
  boolean isItActive(){
    return this.active;
  }
  
  void deactivate(){
    this.active = false;
    this.onOrOff = 0;
  }
  
}
