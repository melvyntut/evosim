class Square{
  
  int x = width/2;
  int y = height/2;
  int w = 20;
  int h = 20;
  int eaten = 0;
  
  int directionLog = 0;
  float leftLimit = 1;
  float rightLimit = 2;
  float downLimit = 3;
  
  void show(){
    
    fill(255);
    rect(x, y, w, h);
    
    float direction = random(4);
    
    //AUTOMATIC MOVEMENT
    if((direction < leftLimit) && x > 0){ //move left
      x = x - 20;
      directionLog = 1;
    } else if((direction > leftLimit && direction < rightLimit) && x < 780){ //move right
      x = x + 20;
      directionLog = 2;
    } else if((direction > rightLimit && direction < downLimit) && y < 780){ //move down
      y = y + 20;
      directionLog = 3;
    } else if((direction > downLimit) && y > 0){ //move up
      y = y - 20;
      directionLog = 4;
    }
    
  }
  
  int getX(){
    return this.x;
  }
  
  int getY(){
    return this.y;
  }
  
  void left(){
    x = x - 20;
  }
  
  void right(){
    x = x + 20;
  }
  
  void down(){
    y = y + 20;
  }
  
  void up(){
    y = y - 20;
  }
  
  //increase the chance of moving in a certain direction when that direction leads to eating food
  int eaten(){
    this.eaten++;
    
    if(directionLog == 1){
      leftLimit = leftLimit + 0.005;
      rightLimit = rightLimit - 0.005;
      downLimit = downLimit - 0.005;
    } else if(directionLog == 2){
      rightLimit = rightLimit + 0.005;
      leftLimit = leftLimit - 0.005;
      downLimit = downLimit - 0.005;
    } else if(directionLog == 3){
      downLimit = downLimit + 0.005;
      leftLimit = leftLimit - 0.005;
      rightLimit = rightLimit - 0.005;
    } else if(directionLog == 4){
      leftLimit = leftLimit - 0.005;
      rightLimit = rightLimit - 0.005;
      downLimit = downLimit - 0.005;
    }
    
    return this.eaten;
  }
  
}
