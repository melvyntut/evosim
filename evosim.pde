Square square;
Food[] foods = new Food[1600];

void setup(){
  
  //frameRate(1);
  size(800, 800);
  square = new Square();
  for(int i = 0; i < 1600; i++){
    foods[i] = new Food();
  }
  
}

int foodIteration = 0;
int rowIteration = 0;
int foodX = 0;
int foodY = 0;

void draw(){
  
  background(0);
  
  //DRAWING LINES
  
  int lineX = 20; int lineY = 20; 
  
  //horizontal lines
  for(int i = 0; i < 40; i++){
    line(lineX, 0, lineX, 800);
    stroke(255);
    lineX = lineX + 20;
  }
  
  //vertical lines
  for(int i = 0; i < 40; i++){
    line(0, lineY, 800, lineY);
    stroke(255);
    lineY = lineY + 20;
  }
  
  //draw forty rows of food objects
  while(rowIteration < 40){
    for(int i = 0; i < 40; i++){
      foods[foodIteration].setPosition(foodX, foodY);
      foodX = foodX + 20;
      foodIteration++;
    }
    foodX = 0;
    foodY = foodY + 20;
    rowIteration++;
  }
  
  //show food objects
  for(int i = 0; i < 1600; i++){
    foods[i].show();
  }
  
  square.show(); //show square objects
  
  //square eats food if they occupy the same space
  for(int i = 0; i < 1600; i++){
    if(square.getX() == foods[i].getX() && square.getY() == foods[i].getY() && foods[i].isItActive()){
      foods[i].eaten();
      foods[i].deactivate();
      println(square.eaten());
    }
  }
  
}

//controls for when automatic movement is disabled
void keyPressed(){
  
  if(keyCode == LEFT){
    square.left();
  } else if(keyCode == RIGHT){
    square.right();
  } else if(keyCode == DOWN){
    square.down();
  } else if(keyCode == UP){
    square.up();
  }
  
}
